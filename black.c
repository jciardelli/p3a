/* a simple next fit allocation followed by a free */
#include <assert.h>
#include <stdlib.h>
#include "mymem.h"

int main() {
  assert(Mem_Init(4096,64) != NULL);
  void* array[32];
  int i;
  for (i = 0; i < 32; i++) {
    array[i] = Mem_Alloc(32);

    assert(array[i] != NULL);
    assert(Mem_Free(array[i]) == 0);
  }

  for (i = 0; i < 32; i++) {
    void* intptr= array[i] + 4;
    assert(*((int *)intptr) != 420007);
  }

  exit(0);
}
