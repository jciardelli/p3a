/******************************************************************************
 * FILENAME: mem.c
 * AUTHOR:   cherin@cs.wisc.edu <Cherin Joseph>
 * MODIFIED BY: Christian Elowsky, Section 001
 * DATE:     20 Nov 2013
 * PROVIDES: Contains a set of library functions for memory allocation
 * *****************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include "mem.h"

/* this structure serves as the header for each block */
typedef struct block_hd{
  /* The blocks are maintained as a linked list */
  /* The blocks are ordered in the increasing order of addresses */
  struct block_hd* next;

  /* size of the block is always a multiple of 4 */
  /* ie, last two bits are always zero - can be used to store other information*/
  /* LSB = 0 => free block */
  /* LSB = 1 => allocated/busy block */

  /* So for a free block, the value stored in size_status will be the same as the block size*/
  /* And for an allocated block, the value stored in size_status will be one more than the block size*/

  /* The value stored here does not include the space required to store the header */

  /* Example: */
  /* For a block with a payload of 24 bytes (ie, 24 bytes data + an additional 8 bytes for header) */
  /* If the block is allocated, size_status should be set to 25, not 24!, not 23! not 32! not 33!, not 31! */
  /* If the block is free, size_status should be set to 24, not 25!, not 23! not 32! not 33!, not 31! */
  int size_status;

}block_header;

/* Global variable - This will always point to the first block */
/* ie, the block with the lowest address */
block_header* list_head = NULL;


/* Function used to Initialize the memory allocator */
/* Not intended to be called more than once by a program */
/* Argument - sizeOfRegion: Specifies the size of the chunk which needs to be allocated */
/* Returns 0 on success and -1 on failure */
int Mem_Init(int sizeOfRegion)
{
  int pagesize;
  int padsize;
  int fd;
  int alloc_size;
  void* space_ptr;
  static int allocated_once = 0;
  
  if(0 != allocated_once)
  {
    fprintf(stderr,"Error:mem.c: Mem_Init has allocated space during a previous call\n");
    return -1;
  }
  if(sizeOfRegion <= 0)
  {
    fprintf(stderr,"Error:mem.c: Requested block size is not positive\n");
    return -1;
  }

  /* Get the pagesize */
  pagesize = getpagesize();

  /* Calculate padsize as the padding required to round up sizeOfRegio to a multiple of pagesize */
  padsize = sizeOfRegion % pagesize;
  padsize = (pagesize - padsize) % pagesize;

  alloc_size = sizeOfRegion + padsize;

  /* Using mmap to allocate memory */
  fd = open("/dev/zero", O_RDWR);
  if(-1 == fd)
  {
    fprintf(stderr,"Error:mem.c: Cannot open /dev/zero\n");
    return -1;
  }
  space_ptr = mmap(NULL, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
  if (MAP_FAILED == space_ptr)
  {
    fprintf(stderr,"Error:mem.c: mmap cannot allocate space\n");
    allocated_once = 0;
    return -1;
  }
  
  allocated_once = 1;
  
  /* To begin with, there is only one big, free block */
  list_head = (block_header*)space_ptr;
  list_head->next = NULL;
  /* Remember that the 'size' stored in block size excludes the space for the header */
  list_head->size_status = alloc_size - (int)sizeof(block_header);
  
  return 0;
}


/* Function for allocating 'size' bytes. */
/* Returns address of allocated block on success */
/* Returns NULL on failure */
/* Here is what this function should accomplish */
/* - Check for sanity of size - Return NULL when appropriate */
/* - Round up size to a multiple of 4 */
/* - Traverse the list of blocks and allocate the best free block which can accommodate the requested size */
/* -- Also, when allocating a block - split it into two blocks when possible */
/* Tips: Be careful with pointer arithmetic */
void* Mem_Alloc(int size)
{
  int const HEADERSIZE = 8; /* 8 bytes for the header of each block */

  /* Grab list_head from global first block, i.e. block with 
   * lowest address */
  block_header* currentBlockPointer = list_head;
  
  /* Pointer to keep track of free block to be used for best fit implementation */
  block_header* bestFit = NULL; /* Should initially be updated to first free block */
  unsigned int bestFitDiff = 65000; /* Should be initially be updated to first free block size */
  
  /* When scanning, use this variable to update bestFitDiff and bestFit correctly. If it's the first free
   * block found, we will automatically set the values of bestFit and bestFitDiff. Otherwise, compare size 
   * of the current block to the bestFit block. */
  int firstFreeBlockFound = 1;
  
  /* Check if size is valid */
  if (size <= 0) {
    fprintf(stderr, "Error, size being allocated must be positive.\n");
    return NULL;  
  }
  
  /* Check if size is too big, including header of a potential new block, (header for list_head is included in 
   * list_head->size_status. */
  /*if (size > list_head->size_status + sizeof(block_header)) {
    fprintf(stderr, "Error, size being allocated is too large.\n");
    return NULL;
  }*/
   
  /* Round up size to a multiple of 4 */
  int remainder = size % 4;
  if (remainder == 0) {
    size = size;  
  }
  else {
    size = size + 4 - remainder;
  }

  /* Scan through free list once to find the best fit free block */
  while (currentBlockPointer != NULL) {
    int currentBlockSize = currentBlockPointer->size_status; /* Update size_status of currentBlockPointer */
                                                              /* Use currentBlockSize for shorter, simpler code */
    /* Check to see if the block being scanned is free and large enough for the request.
     * Bitwise AND with currentBlockSize to detect busy block. */
    if (currentBlockSize & 1 || currentBlockSize < size + HEADERSIZE) {
      currentBlockPointer = currentBlockPointer->next;          
    }
    
    /* Valid free block has been found */
    else {
      
      /* If this is the first valid free block found in the scan, use it as the bestFit */
      if (firstFreeBlockFound) {
        bestFit = currentBlockPointer;
        bestFitDiff = bestFit->size_status - size;
        firstFreeBlockFound = 0;
        currentBlockPointer = currentBlockPointer->next;
      }

      /* Otherwise, compare the current block to the bestFit block */
      else {
        
        /* If currentBlockSize is closer to size requested than bestFit block size, update
         * bestFit and bestFitDiff accordingly */
        if ((currentBlockSize - size) < bestFitDiff) {
          bestFit = currentBlockPointer;
          bestFitDiff = currentBlockSize - size;
          currentBlockPointer = currentBlockPointer->next;
        }
        else {
          currentBlockPointer = currentBlockPointer->next;
        }
      }
    }

    

  } /* End first scan to find best fit free block*/
  
 
  /* If no free block large enough was found, return NULL */
  if (firstFreeBlockFound) {
    fprintf(stderr,"No free block large enough was found.\n");
    return NULL;
  }
  
  /* Find the size of the free space in block after splitting bestFit.
   * Does not include header of bestFit. */ 
  int splitSizeStatus = bestFit->size_status - size - HEADERSIZE;

  /* Make size of bestFit (which will be returned) equal to the rounded size
   * requested from the user. */
  bestFit->size_status = size;
  
  
  /* Split if there is enough for a header and 4 bytes of free space. */
  if (splitSizeStatus > sizeof(block_header) + 4) {
    
    /* Create a new pointer to keep track of free space that remains after
    * allocated space from bestFit */
    block_header* splitPointer = bestFit;
    
    /* Calculate where free block starts using bestFit */
    /* Start at bestFit, add size allocated to bestFit, include bestFit's header */
    /* Note, pointer addresses increase by increments of 8, so we divide size by 8 */
    splitPointer += (size/8) + sizeof(block_header);
    
    /* Insert splitPointer into the linked list */
    splitPointer->next = bestFit->next;
    bestFit->next = splitPointer;
  
    /* Give splitPointer the size_status calculated in splitSizeStatus */
    splitPointer->size_status = splitSizeStatus;

    /* Make sure size_status of splitPointer has LSB 0 */
    splitPointer->size_status = splitPointer->size_status & ~1;

  } 
  
  /* Otherwise, add extra space to current block. Include header that now won't be used. */
  else {
    bestFit->size_status += splitSizeStatus + sizeof(block_header);  
  }

  /* Mark new block as busy */
  bestFit->size_status = bestFit->size_status | 1;
  Mem_Dump();
  return bestFit;
}

/* Function for freeing up a previously allocated block */
/* Argument - ptr: Address of the block to be freed up */
/* Returns 0 on success */
/* Returns -1 on failure */
/* Here is what this function should accomplish */
/* - Return -1 if ptr is NULL */
/* - Return -1 if ptr is not pointing to the first byte of a busy block */
/* - Mark the block as free */
/* - Coalesce if one or both of the immediate neighbours are free */
int Mem_Free(void *ptr)
{
  /* Cast void ptr to block_header and store in blockptr */
  block_header* blockptr = (block_header*)ptr;
  
  /* Check if ptr is null or if ptr is not pointing to a busy block allocated by Mem_Alloc */
  if (blockptr == NULL || (blockptr->size_status & 1) == 0) {
    return -1;
  }

  /* Check if next block is free and not null and coalesce if it is */
  block_header* nextptr = blockptr->next;
  if ((nextptr->size_status & 1) == 0 && (nextptr != NULL)) {
    
    /* Add size of nextptr block and its header to size of blockptr */
    blockptr->size_status += nextptr->size_status + sizeof(block_header);
    
    /* Remove nextptr from list */
    blockptr->next = nextptr->next;
    nextptr->next = NULL;  
  }
  
  /* Use copy of list_head to find previous node */
  block_header* prevptr = list_head;
  
  /* If block has no previous block, i.e. it is list_head, end here */
  if (blockptr == list_head) {
    blockptr->size_status &= ~1;
    Mem_Dump();
    return 0;
  }
  /* Find previous block */
  while (prevptr->next != blockptr) {
    prevptr = prevptr->next;  
  }

  /* Check if the previous block is free and coalesce if it is */
  if ((prevptr->size_status & 1) == 0 && prevptr != NULL) {
    
    /* Add size of blockptr (which should include nextptr if it was free) and
     * its header to size_status of prevptr */
    prevptr->size_status += blockptr->size_status + sizeof(block_header);

    /* Make sure LSB of size_status is 0 to mark as free */
    prevptr->size_status = prevptr->size_status & ~1;
   
    /* Remove blockptr from list */
    prevptr->next = blockptr->next;
    blockptr->next = NULL;

  }
  /* If the previous block is busy, just set the status bit and finish */
  else {
    blockptr->size_status &= ~1;
  }
  Mem_Dump();
  return 0;
}

/* Function to be used for debug */
/* Prints out a list of all the blocks along with the following information for each block */
/* No.      : Serial number of the block */
/* Status   : free/busy */
/* Begin    : Address of the first useful byte in the block */
/* End      : Address of the last byte in the block */
/* Size     : Size of the block (excluding the header) */
/* t_Size   : Size of the block (including the header) */
/* t_Begin  : Address of the first byte in the block (this is where the header starts) */
void Mem_Dump()
{
  int counter;
  block_header* current = NULL;
  char* t_Begin = NULL;
  char* Begin = NULL;
  int Size;
  int t_Size;
  char* End = NULL;
  int free_size;
  int busy_size;
  int total_size;
  char status[5];

  free_size = 0;
  busy_size = 0;
  total_size = 0;
  current = list_head;
  counter = 1;
  fprintf(stdout,"************************************Block list***********************************\n");
  fprintf(stdout,"No.\tStatus\tBegin\t\tEnd\t\tSize\tt_Size\tt_Begin\n");
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  while(NULL != current)
  {
    t_Begin = (char*)current;
    Begin = t_Begin + (int)sizeof(block_header);
    Size = current->size_status;
    strcpy(status,"Free");
    if(Size & 1) /*LSB = 1 => busy block*/
    {
      strcpy(status,"Busy");
      Size = Size - 1; /*Minus one for ignoring status in busy block*/
      t_Size = Size + (int)sizeof(block_header);
      busy_size = busy_size + t_Size;
    }
    else
    {
      t_Size = Size + (int)sizeof(block_header);
      free_size = free_size + t_Size;
    }
    End = Begin + Size;
    fprintf(stdout,"%d\t%s\t0x%08lx\t0x%08lx\t%d\t%d\t0x%08lx\n",counter,status,(unsigned long int)Begin,(unsigned long int)End,Size,t_Size,(unsigned long int)t_Begin);
    total_size = total_size + t_Size;
    current = current->next;
    counter = counter + 1;
  }
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  fprintf(stdout,"*********************************************************************************\n");

  fprintf(stdout,"Total busy size = %d\n",busy_size);
  fprintf(stdout,"Total free size = %d\n",free_size);
  fprintf(stdout,"Total size = %d\n",busy_size+free_size);
  fprintf(stdout,"*********************************************************************************\n");
  fflush(stdout);
  return;
}

