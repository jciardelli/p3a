/* Author: Christian Elowsky
 * Section: 001 
 * */
#include <stdio.h>
#include "mem.h"

int main() {
  /* Allocate 8 bytes for ptr, and make sure it is not null */
  printf("Initializing memory space...\n");
  if (Mem_Init(4096) == -1) {
    printf("Error, could not create initial memory space.\n");
  }

  printf("Allocating 8 bytes...\n");
  void* ptr = Mem_Alloc(8);
  if (ptr == NULL) {
    printf("Error, Mem_Alloc() did not allocate space properly.\n");
  }
  Mem_Dump();
  printf("\n");
  
  /* Free 8 bytes for ptr, and check if it closes without error */
  printf("Freeing 8 bytes...\n");
  if (Mem_Free(ptr) == -1) {
    printf("Error, Mem_Free() could not free space from given pointer.\n");
  }
  Mem_Dump();
  printf("\n");
  
  /* Pass a NULL pointer to Mem_Free */
  printf("Passing in NULL pointer to Mem_Free()...\n");
  if (Mem_Free(NULL) == -1) {
    printf("Error, NULL pointer detected.\n");
  } 
  Mem_Dump();
  printf("\n");
  
  /* Pass an address of memory space not allocated by Mem_Alloc() */
  printf("Passing address of memory space not allocated by Mem_Alloc() to Mem_Free()...\n");
  int example[3] = {1, 2, 3};
  void* badptr = example;
  if(Mem_Free(badptr) == -1) {
    printf("Error, memory space not allocated by Mem_Alloc().\n");
  }
  Mem_Dump();
  printf("\n");
  
  return 0;
}
