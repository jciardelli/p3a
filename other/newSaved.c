
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include "mymem.h"

/* this structure serves as the header for each block */
typedef struct FreeHeader block_header;
typedef struct AllocatedHeader slab_header;

int SlabFree(block_header* blockptr);

void* SlabAlloc(int size);

void* NextFitAlloc(int size);

/* Global variable - This will always point to the first block in the memory Space */
/* ie, the block with the lowest address */
block_header* list_head = NULL;

/* Global variable - This will be set by Mem_Init to indicate whether memory should
 * be allocated by the slab allocator (i.e. equal to specialSize) or next fit 
 * allocator (i.e. not equal to specialSize). */
int specialSize;
 
/* Global variable - This keeps track of the most recently found valid free block from the
 * next fit allocator. */
block_header* nextFitBlock = NULL;

/* Global variable - This stores the total size allocated by Mem_Init. */
int totalAllocatedSpace;

// stores the last address in the free space
void* lastAddress; 

/* Global variable - This stores the start of the nextFit memory region. */
block_header* startOfNextFit = NULL;

// this is the ptr to the next free slab
block_header* nextFreeSlabHeader = NULL;

/* Function used to Initialize the memory allocator */
/* Not intended to be called more than once by a program */
/* Argument - sizeOfRegion: Specifies the size of the chunk which needs to be allocated */
/* Returns 0 on success and -1 on failure */
void* Mem_Init(int sizeOfRegion, int slabSize)
{
  int pagesize;
  int padsize;
  int fd;
  int alloc_size;
  void* space_ptr;
  static int allocated_once = 0;

  if(0 != allocated_once)
  {
    fprintf(stderr,"Error:mem.c: Mem_Init has allocated space during a previous call\n");
    return NULL;
  }
  if(sizeOfRegion <= 0)
  {
    fprintf(stderr,"Error:mem.c: Requested block size is not positive\n");
    return NULL;
  }

  /* Get the pagesize */
  pagesize = getpagesize();

  /* Calculate padsize as the padding required to round up sizeOfRegio to a multiple of pagesize */
  padsize = sizeOfRegion % pagesize;
  padsize = (pagesize - padsize) % pagesize;

  alloc_size = sizeOfRegion + padsize;

  /* Using mmap to allocate memory */
  fd = open("/dev/zero", O_RDWR);
  if(-1 == fd)
  {
    fprintf(stderr,"Error:mem.c: Cannot open /dev/zero\n");
    return NULL;
  }
  space_ptr = mmap(NULL, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
  if (MAP_FAILED == space_ptr)
  {
    fprintf(stderr,"Error:mem.c: mmap cannot allocate space\n");
    allocated_once = 0;
    return NULL;
  }

  allocated_once = 1;

  /* To begin with, there is only one big, free block */
  list_head = (block_header*)space_ptr;
  nextFreeSlabHeader = list_head;
  list_head->next = NULL;

  /* Remember that the 'size' stored in block size excludes the space for the header */
  list_head->length = alloc_size - (int)sizeof(block_header);

  /* Save space allocated by Mem_Init to find the beginning and ends of slab and next fit
   * regions of memory. */
  totalAllocatedSpace = alloc_size;

  // store last address of free space
  lastAddress = space_ptr + totalAllocatedSpace;

  /* Initialize the memory used for the slab allocator to be a quarter of the total space. */
  int slabSpaceSize = totalAllocatedSpace / 4;
  
  /* Find the beginning of the next fit allocator space by counting from the start of the 
   * total allocated space. */
  startOfNextFit = space_ptr + slabSpaceSize;

  /* Store the special size that if used will call the slab allocator instead of the next fit
   * allocator. */
  specialSize = slabSize;

  /* Make sure the first run of the next fit allocator starts at the correct address in memory. */
  nextFitBlock = startOfNextFit;

  return list_head;
}


/* Function for allocating 'size' bytes. */
/* Returns address of allocated block on success */
/* Returns NULL on failure */
/* Here is what this function should accomplish */
/* - Check for sanity of size - Return NULL when appropriate */
/* - Round up size to a multiple of 16 */
/* - Traverse the list of blocks until a free block which can accommodate the requested size is found */
/* -- Also, when allocating a block - split it into two blocks when possible */
/* Tips: Be careful with pointer arithmetic */
void* Mem_Alloc(int size)
{
  // ensure Mem_Init has been called
  if (list_head == NULL) return NULL;

  int const HEADERSIZE = 16; /* 16 bytes for the header of each block */

  /* Pointer to keep track of block to be allocated for next fit implementation */
  block_header* nextFit = nextFitBlock; /* Should initially be updated to last allocated free block */

  /* Used to detect whether a free block is found. */
  int freeBlockFound = 1;

  /* Check if size is valid */
  if (size <= 0) {
    fprintf(stderr, "Error, size being allocated must be positive.\n");
    return NULL;  
  }
  
  // check if <size> is the special size (size of a slab)
  if (size == specialSize)
    // use the ***SLAB ALLOCATOR***
    { 
      return SlabAlloc(size);
    }

  // use the nextFit allocator
  else {

    /* Round up size to a multiple of 16 */
    int remainder = size % 16;
    if (remainder == 0) {
      size = size;  
    }
    else {
      size = size + (16 - remainder);
    }

    /* Grab last block allocated from global nextFreeBlock. */
    block_header* currentBlockPointer = nextFreeBlock;

    /* Scan through free list once to find the next fit free block */
    while (currentBlockPointer != NULL) {

      /* Update length of currentBlockPointer. Use currentBlockSize for shorter, simpler code */
      int currentBlockSize = currentBlockPointer->length; 

      /* Check to see if the block being scanned is free and large enough for the request.
       * Bitwise AND with currentBlockSize to detect busy block. */
      if (currentBlockSize & 1 || currentBlockSize < size + HEADERSIZE) {
        currentBlockPointer = currentBlockPointer->next;          
      }

      /* Valid free block has been found. */
      else {
        nextFit = currentBlockPointer;
        freeBlockFound = 0;
        currentBlockPointer = currentBlockPointer->next;

        /* If we have not reached the end of the next fit list, update the next fit previous pointer
         * nextFreeBlock. */
        if (currentBlockPointer != NULL) {
          nextFreeBlock = currentBlockPointer;
        }

        /* If we have reached the end of the next fit list, start at the beginning. */ 
        else {
          currentBlockPointer = startOfNextFit;
          while (currentBlockPointer != NULL) {
            currentBlockSize = currentBlockPointer->length;
            
            /* Check to see if the block being scanned is free and large enough for the request.
             * Bitwise AND with currentBlockSize to detect busy block. */
            if (currentBlockSize & 1 || currentBlockSize < (size + HEADERSIZE)) {
              currentBlockPointer = currentBlockPointer->next;          
            }

            /* There is no more free memory in the next fit list.  */
            else {
              nextFreeBlock = currentBlockPointer->next; // ***************** this is not necessarily free FIX THIS******
              return currentBlockPointer + sizeof(block_header);
            }
          }
        }

        /* Save the found free block in the global variable nextFreeBlock for the next run of 
         * next fit allocation. */
        nextFreeBlock = currentBlockPointer;

      }
    }

    /* If no free block large enough was found, return NULL */
    if (freeBlockFound) {
      fprintf(stderr,"No free block large enough was found.\n");
      return NULL;
    }

    /* Find the size of the free space in block after splitting nextFit.
     * Does not include header of nextFit. */ 
    int splitLength = nextFit->length - size - HEADERSIZE;

    /* Make size of nextFit (which will be returned) equal to the rounded size
     * requested from the user. */
    nextFit->length = size;

    /* Split if there is enough for a header and 16 bytes of free space. */
    if (splitLength > sizeof(block_header) + 16) {

      /* Create a new pointer to find free space that remains after
       * allocated space from nextFit. Using void* for easier address finding */
      void* temp = nextFit;

      /* Calculate where free block starts using nextFit's address in temp */
      /* Start at nextFit, add size allocated to nextFit, include nextFit's header */
      temp += size + sizeof(block_header);

      /* Cast temp to block_header* and store in splitPointer to change length 
       * and next fields */
      block_header* splitPointer = (block_header*)temp;

      /* Insert splitPointer into the linked list */
      splitPointer->next = nextFit->next;
      nextFit->next = splitPointer;

      /* Give splitPointer the length calculated in splitLength */
      splitPointer->length = splitLength;

      /* Make sure length of splitPointer has LSB 0 */
      splitPointer->length = splitPointer->length & ~1;

    } 

    /* Otherwise, add extra space to current block. Include header of potential new block
     * that now won't be used. */
    else {
      nextFit->length += splitLength + sizeof(block_header);  
    }

    /* Mark new block as busy */
    nextFit->length = nextFit->length | 1;
    return nextFit;
  }

}

/* Function for freeing up a previously allocated block */
/* Argument - ptr: Address of the block to be freed up */
/* Returns 0 on success */
/* Returns -1 on failure */
/* Here is what this function should accomplish */
/* - Return -1 if ptr is NULL */
/* - Return -1 if ptr is not pointing to the first byte of a busy block */
/* - Mark the block as free */
/* - Coalesce if one or both of the immediate neighbours are free */
int Mem_Free(void* ptr)
{
  /* Cast void ptr to block_header and store in blockptr */
  block_header* blockptr = (block_header*)ptr;

  // check if ptr is null
  if (blockptr == NULL) return -1;
 
  // check if ptr is outside mmapped range
  if (blockptr < list_head || blockptr > (block_header*)lastAddress) {
    fprintf(stderr, "SEGFAULT\n");
    return -1;
  }
  
  // check if ptr is in the slab space
  if (blockptr < startOfNextFit){
    // *** FREE SLAB ***
    return SlabFree(blockptr);
  }
  // *** FREE NEXT FIT ***

  if (blockptr < startingAddress || blockptr > lastAddress ) {
    fprintf(stderr, "SEGFAULT\n");u    return -1;
  }

  /* Check if next block is free and not null and coalesce if it meets these requirements */
  block_header* nextptr = blockptr->next;
  if ((nextptr->length & 1) == 0 && (nextptr != NULL)) {

    /* Add size of nextptr block and its header to size of blockptr */
    blockptr->length += nextptr->length + sizeof(block_header);

    /* Remove nextptr from list */
    blockptr->next = nextptr->next;
    nextptr->next = NULL;  
  }

  /* Use copy of startOfNextFit to find previous node */
  block_header* prevptr = startOfNextFit;

  /* If block has no previous block, i.e. it is list_head, end here */
  if (blockptr == startOfNextFit) {
    blockptr->length &= ~1; // bitwise AND with the complement of 1 to set status to 0
    return 0;
  }

  /* Find previous block */
  while (prevptr->next != blockptr) {
    prevptr = prevptr->next;  
  }

  /* Check if the previous block is free and coalesce if it is */
  if ((prevptr->length & 1) == 0 && prevptr != NULL) {

    /* Add size of blockptr (which should include nextptr if it was free) and
     * its header to length of prevptr */
    prevptr->length += blockptr->length + sizeof(block_header);

    /* Make sure LSB of length is 0 to mark as free */
    prevptr->length = prevptr->length & ~1;

    /* Remove blockptr from list */
    prevptr->next = blockptr->next;
    blockptr->next = NULL;
  }

  /* If the previous block is busy, just set the status bit and finish */
  else {
    blockptr->length &= ~1;
  }
  return 0;
}
  


int SlabFree (block_header* blockptr){  

  // check if pointer is a multiple of the special size rounded to a 16 byte aligned chunk
  int slabSize = specialSize;
  int remainder = specialSize % 16;
  if (remainder != 0){
    slabSize = specialSize + (16 - remainder);
  }
  
  if (((blockptr - list_head) % slabSize) != 0) return -1;
  
  // navigate to end of free slab space linked list
  block_header* temp = nextFreeSlabHeader;
  while (temp->next != NULL) {
    temp = temp->next;
  }
  // add the new slab to the end of the free space list
  temp->next = blockptr;
  
  return 0;
}


void* SlabAlloc (int size){
  // round up to 16 aligned chunk
  int remainder = size % 16;
  int newSize = size;
  if (remainder != 0)
    {
      newSize = size + (16 - remainder);
    }
  // return the slab if there is space, and update the pointer to the next free slab 
  char* temp = (char *)nextFreeSlabHeader;
  temp = temp + newSize; 
  block_header* newNextFreeSlabHeader = (block_header*) temp;
  if (nextFreeSlabHeader != NULL && newNextFreeSlabHeader < startOfNextFit)
    {
      // declare the void pointer to return
      void* slab = nextFreeSlabHeader;
      
      // remove the slab from the linked list of free slabs 
      newNextFreeSlabHeader->next = nextFreeSlabHeader->next; 
      nextFreeSlabHeader = newNextFreeSlabHeader;

      return slab;
    }
  // if slab space has been filled, check the linked list for freed slabs
  else if (nextFreeSlabHeader != NULL && nextFreeSlabHeader->next != NULL){
    nextFreeSlabHeader = nextFreeSlabHeader->next;
    void* slab = nextFreeSlabHeader;
    nextFreeSlabHeader = nextFreeSlabHeader->next;
    return slab;
  }
  // last, try the nextFit allocator
  return NextFitAlloc(size);
}

void* NextFitAlloc(int size){
  return NULL;
}




/* Function to be used for debug */
/* Prints out a list of all the blocks along with the following information for each block */
/* No.      : Serial number of the block */
/* Status   : free/busy */
/* Begin    : Address of the first useful byte in the block */
/* End      : Address of the last byte in the block */
/* Size     : Size of the block (excluding the header) */
/* t_Size   : Size of the block (including the header) */
/* t_Begin  : Address of the first byte in the block (this is where the header starts) */
void Mem_Dump()
{
  int counter;
  block_header* current = NULL;
  char* t_Begin = NULL;
  char* Begin = NULL;
  int Size;
  int t_Size;
  char* End = NULL;
  int free_size;
  int busy_size;
  int total_size;
  char status[5];

  free_size = 0;
  busy_size = 0;
  total_size = 0;
  current = list_head;
  counter = 1;
  fprintf(stdout,"************************************Block list***********************************\n");
  fprintf(stdout,"No.\tStatus\tBegin\t\tEnd\t\tSize\tt_Size\tt_Begin\n");
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  while(NULL != current)
  {
    t_Begin = (char*)current;
    Begin = t_Begin + (int)sizeof(block_header);
    Size = current->length;
    strcpy(status,"Free");
    if(Size & 1) /*LSB = 1 => busy block*/
    {
      strcpy(status,"Busy");
      Size = Size - 1; /*Minus one for ignoring status in busy block*/
      t_Size = Size + (int)sizeof(block_header);
      busy_size = busy_size + t_Size;
    }
    else
    {
      t_Size = Size + (int)sizeof(block_header);
      free_size = free_size + t_Size;
    }
    End = Begin + Size;
    fprintf(stdout,"%d\t%s\t0x%08lx\t0x%08lx\t%d\t%d\t0x%08lx\n",counter,status,(unsigned long int)Begin,(unsigned long int)End,Size,t_Size,(unsigned long int)t_Begin);
    total_size = total_size + t_Size;
    current = current->next;
    counter = counter + 1;
  }
  fprintf(stdout,"---------------------------------------------------------------------------------\n");
  fprintf(stdout,"*********************************************************************************\n");

  fprintf(stdout,"Total busy size = %d\n",busy_size);
  fprintf(stdout,"Total free size = %d\n",free_size);
  fprintf(stdout,"Total size = %d\n",busy_size+free_size);
  fprintf(stdout,"*********************************************************************************\n");
  fflush(stdout);
  return;
}

