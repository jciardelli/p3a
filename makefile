mem: mem.c mymem.h
	gcc -c -fpic mem.c -Wall -Werror -g
	gcc -shared -o libmem.so mem.o -g
	gcc -lmem -L. -pthread -o threaded threaded.c -Wall -Werror -g

clean:
	rm -rf mem.o libmem.so
	rm -rf *~
